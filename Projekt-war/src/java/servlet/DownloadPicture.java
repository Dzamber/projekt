package servlet;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import exampole.IconsManagment_Service;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author student
 */
@WebServlet(urlPatterns = {"/DownloadPicture"})
public class DownloadPicture extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/iconsManagment/iconsManagment.wsdl")
    private IconsManagment_Service service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id_download");
        List<String> content = getContentToDownload(id);
        
        response.setContentType("application/octet-stream");
        byte[] dane = Base64.getDecoder().decode(content.get(1));
        response.setContentLength((int) dane.length);
        //System.out.println("Content to: " + content.get(1));
        response.setHeader("Content-Disposition", "attachment; filename=\"" + content.get(0) + "\"");
        
        OutputStream out = response.getOutputStream();
        out.write(dane);
        out.close();
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/mainSite.jsp").forward(
         request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private java.util.List<java.lang.String> getContentToDownload(String id) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        System.out.println("DownloadPicture private getContentToDownload " + id);
        exampole.IconsManagment port = service.getIconsManagmentPort();
        return port.getContentToDownload(id);
    }
}
