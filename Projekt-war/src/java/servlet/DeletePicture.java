/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import exampole.IconsManagment_Service;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author student
 */
@WebServlet(urlPatterns = {"/DeletePicture"})
public class DeletePicture extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/iconsManagment/iconsManagment.wsdl")
    private IconsManagment_Service service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        deleteFile(request.getParameter("id_delete"));
        getServletContext().getRequestDispatcher("/mainSite.jsp").forward(
         request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/mainSite.jsp").forward(
         request, response);
        //dziala
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    private void deleteFile(java.lang.String id) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        exampole.IconsManagment port = service.getIconsManagmentPort();
        port.deleteFile(id);
    }

}
