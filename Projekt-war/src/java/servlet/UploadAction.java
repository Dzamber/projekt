/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import beans.DbManager;
import exampole.IconsManagment_Service;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.ws.WebServiceRef;
/**
 *
 * @author student
 */
@WebServlet(name = "UploadAction", urlPatterns = {"/UploadAction"})
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2 MB
                 maxFileSize=1024*1024*10,      // 10MB
                 maxRequestSize=1024*1024*50)   // 50MB
public class UploadAction extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/iconsManagment/iconsManagment.wsdl")
    private IconsManagment_Service service;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
           
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {    
        //plik i opis
        Part part = request.getPart("file");
        String description = request.getParameter("desc");
        String fileName = part.getSubmittedFileName();
        byte[] bytes = new byte[(int)part.getSize()];
        try {
            part.getInputStream().read(bytes);
        } catch (IOException ex) {
            Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String extension = fileName.substring(fileName.lastIndexOf(".")+1);
        if(extension.equals("png") ||  extension.equals("jpg")){
            uploadPicture(fileName, description, bytes);
        }
        getServletContext().getRequestDispatcher("/mainSite.jsp").forward(
         request, response);
    }
   

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    private void uploadPicture(java.lang.String filename, java.lang.String description, byte[] picture) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        exampole.IconsManagment port = service.getIconsManagmentPort();
        port.uploadPicture(filename, description, picture);
    }
}

