package servlet;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import beans.Icons;
import exampole.IconsManagment_Service;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author student
 */
@WebServlet(urlPatterns = {"/preview"})
public class preview extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/iconsManagment/iconsManagment.wsdl")
    private IconsManagment_Service service;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<styles>  <link rel='stylesheet' href='Style/css/components.css'>\n" +
            "<link rel='stylesheet' href='Style/css/icons.css'>\n" +
            "<link rel='stylesheet' href='Style/css/responsee.css'>"
             + "<style>\n" +
            "img,h2,h1{\n" +
            "width:100%;\n" +
            "max-width:600px;\n" +
            "max-height:600px;\n" +
            "display: block;\n" +
            "margin-left: auto;\n" +
            "margin-right: auto;\n" +
            "}\n" +
            "</style>"
                    + "</styles>");
            int id = Integer.parseInt(request.getParameter("id_preview"));
            out.println("<title>"+getNameById(id)+"</title>");       
            System.out.println(request.getParameter("id_preview"));
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>"+getNameById(id)+"</h1>");
            out.println("<img src='data:image/jpeg;base64," + getImageById(id) + "'>");
            out.println("\n");
            out.println("<h2>"+getDescriptionById(id)+"<h2>");
            out.println("<form method=\"POST\" action=\"mainSite.jsp\" enctype=\"multipart/form-data\" >\n" +
"                <input  class=\"button cancel-btn\" href=\"#\" type=\"submit\" value=\"Wroc do galerii\"/>\n" +
"                </form>");
            out.println("<form action='DownloadPicture'>");
                            out.println("<input class='button' type='submit' value='Sciagnij' />");
                            out.println("<input type='hidden' name='id_download' value=" + id + " />");
                        out.println("</form>"); 
                        out.println("<form action='DeletePicture'>");
                            out.println("<input class='button cancel-btn margin-bottom' type='submit' value='Usun' />");
                            out.println("<input type='hidden' name='id_delete' value=" + id + " />");
                        out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String getDescriptionById(int id) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        exampole.IconsManagment port = service.getIconsManagmentPort();
        return port.getDescriptionById(id);
    }

    private String getImageById(int id) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        exampole.IconsManagment port = service.getIconsManagmentPort();
        return port.getImageById(id);
    }

    private String getNameById(int id) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        exampole.IconsManagment port = service.getIconsManagmentPort();
        return port.getNameById(id);
    }


}
