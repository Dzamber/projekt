<%-- 
    Document   : upload
    Created on : 2021-02-15, 06:51:57
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel='stylesheet' href='Style/css/components.css'>
        <link rel='stylesheet' href='Style/css/icons.css'>
        <link rel='stylesheet' href='Style/css/responsee.css'>
        <style>
        .outer-outline {
            outline: 5px outset grey;
            outline-offset: 5px;
            width: 300px;
            height: 255px;
		    margin-left:20px;
        }

	.inner-outline {
            outline: 5px inset black;
            outline-offset: 2px;
            width: 298px;
            height: 253px;
            margin-left:2px;
        }
        </style>
    </head>
    <body>
        <h1>Dodawanie emotikony</h1>
        <div class="outer-outline">
            <div class="form_cont inner-outline" border-style="outset">
                <form action="UploadAction" method="post" enctype="multipart/form-data">
                    Plik graficzny:<br/>
                    <label class="button" disabled="true" style="padding: 0; overflow: hidden; width: 280px">
                        <input 
                        required
                        id="file"
                        type="file" 
                        accept="image/png" 
                        name="file" 
                        multiple="false"/>
                    </label><br/><br/>
                    Opis(max 100 znakow):<br/>
                    <textarea class="text_input" name="desc" maxlength="95" required ></textarea><br/><br/>
                    <input class="button" type="submit" value="Dodaj!"><br/>         
                </form>
                <form method="POST" action="mainSite.jsp" enctype="multipart/form-data" >
                <input  class="button cancel-btn margin-bottom" href="#" type="submit" value="Wroc do galerii"/>
                </form>
            </div>
        </div>
    </body>

</html>
