/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.ejb.Remote;
import javax.servlet.http.Part;

/**
 *
 * @author student
 */
@Remote
public interface DbManagerRemote {

    String getDataInDerby();
    void createNewRow(String name, String image, String description);

    void deleteRowById(String id);

    String[] getFileInfo(String id);

    Object getInDerbyByID(int id);

    String getImageById(int id);

    String getDescriptionById(int id);

    String getNameById(int id);


    
}
