/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.servlet.http.Part;

/**
 *
 * @author student
 */
@Stateless
public class DbManager implements DbManagerRemote {
    public static final String DRIVER = "org.apache.derby.jdbc.ClientDriver";
    public static final String JDBC_URL = "jdbc:derby://localhost:1527/Icons";

  
    @Override
    public String getDataInDerby() {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projekt-ejbPU");
        EntityManager em = emf.createEntityManager();
        TypedQuery<Icons> query = em.createNamedQuery("Icons.findAll", Icons.class);
        //List<Icons> results = ;
        
        String listToHTML = Icons.listToHTML(query.getResultList());
        System.out.println(query.getResultList());
        return listToHTML;
    }

    @Override
    public void createNewRow(String name, String image, String description) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projekt-ejbPU");
        EntityManager em = emf.createEntityManager();
        int id = 1; 
        Object number = em.createQuery("SELECT max(i.id) FROM Icons i").getSingleResult();
        if(number == null)
            id = 1;
        else id = (Integer) number + 1;
        //em.getTransaction().begin();
        Icons ic = new Icons();
        ic.setDescription(description);
        ic.setImage(image);
        ic.setName(name);
        ic.setId(id);
        em.persist(ic);
        //em.getTransaction().commit();
        
    }

    @Override
    public void deleteRowById(String id) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projekt-ejbPU");
        EntityManager em = emf.createEntityManager();

        em.remove(em.find(Icons.class, Integer.parseInt(id)));
    }

    @Override
    public String[] getFileInfo(String id) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projekt-ejbPU");
        EntityManager em = emf.createEntityManager();
        Icons toReturn = em.find(Icons.class, Integer.parseInt(id));
        return new String[] {toReturn.getName(), toReturn.getImage()};
    }

    @Override
    public Object getInDerbyByID(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projekt-ejbPU");
        EntityManager em = emf.createEntityManager();
        return (Object) em.find(Icons.class, id);
    }

    @Override
    public String getImageById(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projekt-ejbPU");
        EntityManager em = emf.createEntityManager();
        return em.find(Icons.class, id).getImage();
    }

    @Override
    public String getDescriptionById(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projekt-ejbPU");
        EntityManager em = emf.createEntityManager();
        return em.find(Icons.class, id).getDescription();
    }

    @Override
    public String getNameById(int id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projekt-ejbPU");
        EntityManager em = emf.createEntityManager();
        return em.find(Icons.class, id).getName();
    }
    
    
    
    
}
