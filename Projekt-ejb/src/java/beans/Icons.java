/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author student
 */
@Entity
@Table(name = "ICONS")
@NamedQueries({
    @NamedQuery(name = "Icons.findAll", query = "SELECT i FROM Icons i"),
    @NamedQuery(name = "Icons.findById", query = "SELECT i FROM Icons i WHERE i.id = :id"),
    @NamedQuery(name = "Icons.findByName", query = "SELECT i FROM Icons i WHERE i.name = :name"),
    @NamedQuery(name = "Icons.findByDescription", query = "SELECT i FROM Icons i WHERE i.description = :description")})
public class Icons implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @Lob
    @Column(name = "IMAGE")
    private String image;

    public Icons() {
    }

    public Icons(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Icons)) {
            return false;
        }
        Icons other = (Icons) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "";
    }
    
    public static String listToHTML(List<Icons> list){
        String wiersz;
         wiersz = ("<table><tr>");
         wiersz = wiersz.concat(
         "<td><b>ID</b></td>"
         + "<td><b>Name</b></td>"
         + "<td><b>Description</b></td>"
         + "<td><b>Image</b></td>"
         + "<td><b>Usun lub sciagnij</b></td>");
         wiersz = wiersz.concat("</tr>");
         for (Icons ic : list) {
            wiersz = wiersz.concat("<tr>");
            wiersz = wiersz.concat("<td>" + ic.getId() + "</td>");
            wiersz = wiersz.concat("<td>" + ic.getName() + "</td>");
            wiersz = wiersz.concat("<td>" + ic.getDescription() + "</td>");
            
            wiersz = wiersz.concat("<td>"
                    + "<form action='preview'>"
                    + "<input type='hidden' name='id_preview' value=" + ic.getId() + " />"
                    + "<input type='submit' value='Podglad' class='button'/>"
                    + "<img src='data:image/jpeg;base64," + ic.getImage() + "'>"
                    + "</form>"
                    + "</td>");
            wiersz = wiersz.concat("<td>");
                        wiersz = wiersz.concat("<form action='DownloadPicture'>");
                            wiersz = wiersz.concat("<input class='button' type='submit' value='Sciagnij' />");
                            wiersz = wiersz.concat("<input type='hidden' name='id_download' value=" + ic.getId() + " />");
                        wiersz = wiersz.concat("</form>"); 
                        wiersz = wiersz.concat("<form action='DeletePicture'>");
                            wiersz = wiersz.concat("<input class='button cancel-btn margin-bottom' type='submit' value='Usun' />");
                            wiersz = wiersz.concat("<input type='hidden' name='id_delete' value=" + ic.getId() + " />");
                        wiersz = wiersz.concat("</form>");
                    wiersz = wiersz.concat("</td>");
                    wiersz = wiersz.concat("</tr>");
            wiersz = wiersz.concat("</tr>");
         }
         wiersz = wiersz.concat("</table>");
         return wiersz;
    }
}
