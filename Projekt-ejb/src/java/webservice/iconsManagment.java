/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import beans.DbManager;
import beans.DbManagerRemote;
import java.util.Base64;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.jws.Oneway;

/**
 *
 * @author student
 */
@WebService(serviceName = "iconsManagment")
@Stateless()
public class iconsManagment {

    @EJB
    private DbManagerRemote dbManager = new DbManager();

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "uploadPicture")
    @Oneway
    public void uploadPicture(@WebParam(name = "filename") String filename, @WebParam(name = "description") String description, @WebParam(name = "picture") byte[] picture) {
        //String name = filename.substring(filename.lastIndexOf('.'));
        String fileInBase64 = Base64.getEncoder().encodeToString(picture);
        dbManager.createNewRow(filename, fileInBase64, description);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getPictures")
    public String getPictures() {
        //TODO write your implementation code here:
        return dbManager.getDataInDerby();
    }
    
    @WebMethod(operationName = "getContentToDownload")
    public String[] getContentToDownload(@WebParam(name = "id") String id) {
        System.out.println("Webservice id obrazka: " + id);
        return dbManager.getFileInfo(id);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "deleteFile")
    @Oneway
    public void deleteFile(@WebParam(name = "id") String id) {
        dbManager.deleteRowById(id);
    }
    
    @WebMethod(operationName = "showSingleImage")
    public Object showSingleImage(@WebParam(name = "id") int id) {
        return dbManager.getInDerbyByID(id);
    }
    
    @WebMethod(operationName = "getNameById")
    public String getNameById(@WebParam(name = "id") int id) {
        return dbManager.getNameById(id);
    }
    
    @WebMethod(operationName = "getDescriptionById")
    public String getDescriptionById(@WebParam(name = "id") int id) {
        return dbManager.getDescriptionById(id);
    }
    
    @WebMethod(operationName = "getImageById")
    public String getImageById(@WebParam(name = "id") int id) {
        return dbManager.getImageById(id);
    }
    
    
}
